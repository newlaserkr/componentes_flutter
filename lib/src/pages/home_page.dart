import 'package:flutter/material.dart';

import 'package:componentes_flutter/src/providers/menu_provider.dart';
import 'package:componentes_flutter/src/utils/icono_string_util.dart';
import 'package:componentes_flutter/src/pages/alert_page.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Componentes"),
        ),
        body: _lista());
  }

  Widget _lista() {
    //metodos no viables
    //print(menuProvider.opciones);
    /*menuProvider.cargarDatos().then((opciones) {
      print('Lista');
      print(opciones);
    });*/

    return FutureBuilder(
      future: menuProvider.cargarDatos(),
      initialData: [],
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
        //print("builder");
        //print(snapshot.data);
        return ListView(
          children: _listItems(snapshot.data, context),
        );
      },
    );
  }

  List<Widget> _listItems(List<dynamic> data, BuildContext context) {
    final List<Widget> opciones = [];

    /*if (data == null) {
      return [];
    }*/

    data.forEach((opci) {
      final widgetsTemp = ListTile(
        title: Text(opci['texto']),
        leading: getIcon(opci['icon']),
        trailing: Icon(Icons.keyboard_arrow_right, color: Colors.blue),
        onTap: () {
          Navigator.pushNamed(context, opci['ruta']);
        },
      );
      opciones..add(widgetsTemp)..add(Divider());
    });
    return opciones;
  }
}
