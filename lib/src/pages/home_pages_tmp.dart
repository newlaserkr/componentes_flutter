import 'package:flutter/material.dart';

class HomePagesTmp extends StatelessWidget {
  final opciones = ['Uno', 'dos', 'tres', 'cuatro', 'cinco'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Componentes"),
      ),
      body: ListView(
        //children: _crearItem(),
        children: _crearItemsCorta(),
      ),
    );
  }

  /* List<Widget> _crearItem() {
    List<Widget> lista = new List<Widget>();
    for (String opci in opciones) {
      final tempWidgets = ListTile(
        title: Text(opci),
      );
      lista..add(tempWidgets)..add(Divider());
    }
    return lista;
  }*/

  List<Widget> _crearItemsCorta() {
    return opciones.map((item) {
      return Column(
        children: [
          ListTile(
            title: Text(item + '!'),
            subtitle: Text('Cualquier cosa'),
            leading: Icon(Icons.account_balance_wallet),
            trailing: Icon(Icons.keyboard_arrow_right),
            onTap: () {},
          ),
          Divider()
        ],
      );
    }).toList();
  }
}
