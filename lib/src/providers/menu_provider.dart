import 'package:flutter/services.dart' show rootBundle; //permite leer json
import 'dart:convert';

class _MenuProvider {
  List<dynamic> opciones = [];
  _MenuProvider() {
    //cargarDatos();
  }

  Future<List<dynamic>> cargarDatos() async {
    final resp = await rootBundle.loadString('datajson/menu_opts.json');
    Map dataMap = json.decode(resp);
    print(dataMap['rutas']);
    opciones = dataMap['rutas'];
    return opciones;
  }
}

/*
 * Crear instancia del menu
 */
final menuProvider = new _MenuProvider();
